import argparse
import numpy as np
from pymoo.model.problem import Problem
import bobyqa
from util import get_element_types, round_chr, init_chromosome_v1, init_chromosome_v2, make_movies
from linemaker import linemaker
import pickle
import datetime
import os
from multiprocessing import Pool
from functools import partial
from usecases import Case

np.random.seed(99)

#TODO
# benchmark tests
# simple FODO of a few cells, no bend
# achromat bend
# arc, with significant bending angle only horizontal
# dogleg
# transport to target, meaning alpha=0 and small equal betas, no bends
# one long problem, like the FCC

#TODO
# check relu on strengths

case = Case('sps_arc_cell')

class MyProblem(Problem):

    def __init__(self, chrlength, chr_lims, run_folder, case, parsed_args,
                 *args, **kwargs):
        super().__init__(n_var=chrlength,
                         n_obj=1,
                         n_constr=0,
                         xl = chr_lims[0],
                         xu = chr_lims[1])

        self.maker = linemaker
        self.chrlength = chrlength
        self.plot_every = parsed_args.plot_every
        self.run_folder = run_folder
        self.parsed_args = parsed_args
        self.history = [[],[]]
        self.generation = 0
        self.pool = Pool(self.parsed_args.pool_size)

        # initialization of the pool of processes, each with one linemaker object
        _ = self.pool.map(
            partial(self._linemaker_process_init,
                    run_folder=self.run_folder, chr_type=self.parsed_args.chr_type,
                    drifts_loc=case.drifts, Hbends=case.Hbends, Vbends=case.Vbends, quads=case.quads),
            [i for i in range(self.parsed_args.pool_size)]
        )

    @staticmethod
    def _linemaker_process_init(_, run_folder, chr_type,
                                drifts_loc, Hbends, Vbends, quads,):
        global maker # trick to keep the variable in calls of the process
        maker = linemaker(run_folder, chr_type, case)

    @staticmethod
    def _linemaker_process_eval(chromosome):
        return maker.eval(chromosome)

    @staticmethod
    def _linemaker_process_eval_plot_dump_print(
            eval=False, make_plots=False, dump_output=False, end_print=False,
            chromosome=None, history=None, generation=None
    ):
        if eval:
            maker.eval(chromosome)
        if make_plots:
            maker.make_plots(history, generation)
        if dump_output:
            maker.dump_output(history, chromosome)
        if end_print:
            maker.end_print()

    def _evaluate(self, chr, out, *args, **kwargs):

        self.generation += 1

        err = np.array(self.pool.map(
            self._linemaker_process_eval,
            [x for x in chr]))
        #print('here err is ', err)
        this_best_chr = chr[np.argmin(err)]

        if self.plot_every > 0 and self.generation > 2 and self.generation % self.plot_every == 0:
            if np.min(err) < np.min(self.history[0]):
                self.best_chr = this_best_chr
            self.pool.apply(self._linemaker_process_eval_plot_dump_print,
                            kwds=dict(eval=True, make_plots=True, dump_output=True,
                                      chromosome=this_best_chr, history=self.history, generation=self.generation)
            )

        self.history[0].append(np.min(err))
        self.history[1].append(np.mean(err))

        out["F"] = err #f1
        out["G"] = -err #g1 TODO sort out properly...should not be needed (constraint list)

        print('gen.# : {}, all-time-best : {}, gen.min : {}, gen.aver : {}'.format(
            self.generation, round(np.min(self.history[0]), 4), round(np.min(err), 4), round(np.mean(err),4)))


def main():

    parser = argparse.ArgumentParser(description='Use genetic algorithm for TL design')

    # genetic algorithm parameters
    parser.add_argument('--algorithm', default='CMAES', help='Genetic Algorithm', type=str)
    parser.add_argument('--gen', default=2000,  help='Number of generations', type=int)
    parser.add_argument('--pop', default=500, help='Population per generation', type=int)

    # chromosome type and length parameters
    parser.add_argument('--chr_type', default='v1', help='genome decoding protocol', type=str)
    parser.add_argument('--num_e', default=40, help='number of element genes in genome', type=int)

    # plotting and logging
    parser.add_argument('--plot_every', default=50, help='save intermediate results every n generations (0 = never)', type=int)

    # pool size for the madx processes
    parser.add_argument('--pool_size', default=2, help='Pool size for parallel madx jobs', type=int)

    args, other_args = parser.parse_known_args()

    # some file handling stuff
    run_folder = os.path.join('../data/run', args.algorithm + '_' + args.chr_type + '_' + datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))
    os.makedirs(run_folder)

    fn = os.path.join(run_folder, 'args.pkl')

    with open(fn, 'wb') as handle:
        pickle.dump(args, handle, protocol=pickle.HIGHEST_PROTOCOL)

    # initilise chromosome #TODO hide this inside the linemaker class
    if args.chr_type == 'v1':
        chr, chr_lims, chr_stds, element_types = init_chromosome_v1(
            args.num_e, case.drifts, case.Hbends, case.Vbends, case.quads)
    elif args.chr_type == 'v2' or args.chr_type == 'v3':
        dummydrifts = () #TODO correct drifts variable still passed to V2/3 if this line not included
        chr, chr_lims, chr_stds, element_types = init_chromosome_v2(
            args.num_e, dummydrifts, case.Hbends, case.Vbends, case.quads)
    else:
        raise ValueError('chromosome type not known.')

    problem = MyProblem(len(chr), chr_lims, run_folder, case, args)

    x = np.random.uniform(low=-1, high=1, size=((len(chr))))

    MSEthresh = 10
    upper = np.ones(len(chr)) * 1.0
    lower = np.ones(len(chr)) * -1.0

    def loss(x):
        x = np.reshape(x, (1, -1))
        problem._evaluate(x, ())
        img = generator.predict(x)
        err = MSE(img, ref_img_norm)
        return err

    soln = pybobyqa.solve(loss, x,
                          user_params={"model.abs_tol": MSEthresh},
                          bounds=(lower, upper),
                          maxfun=args.gen,
                          seek_global_minimum=True,
                          rhobeg=0.2)

    print("Best solution found: \nX = %s\nF = %s" % (soln.x, soln.f))

    print("Starting {} with genome {}, for {} generations and {} individuals per generation".
          format(args.algorithm, args.chr_type, args.gen, args.pop))

    problem.pool.apply(
        problem._linemaker_process_eval_plot_dump_print,
        kwds=dict(dump_output=True, make_plots=True, end_print=True,
                  history=problem.history, generation=args.gen+1, chromosome=res.X)
    )

if __name__ == "__main__":
    main()
