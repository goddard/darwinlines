import numpy as np
import time
import os
import pickle
from cpymad import libmadx
from cpymad.madx import Madx
from matplotlib import pyplot as plt
import matplotlib as mpl
from util import get_element_types, check_current, perfect_fodo_start_twiss, norm_parm, round_chr, draw_synoptic

MAX_MADX_LATS_COUNTER = 1000
MAXLENGTH =  1200 #max line length in m

class linemaker():
    metadata = {'render.modes': ['human']}

    def __init__(self, run_folder, chr_type, case, relu,
                 lowlevel_madx=True, suppress_madx_output=True,
                 verbose = False):

        self.epsilon = 0.0
        self.run_folder = run_folder
        self.chr_type = chr_type
        self.relu = abs(relu)

        self.verbose = verbose

        self.lowlevel_madx = lowlevel_madx
        self.suppress_madx_output = suppress_madx_output

        self.beam_rigidity = case.beam_rigidity
        self.pc = self.beam_rigidity / 3.335639951

        self.twiss0 = case.twiss0
        self.twissf = case.twissf
        self.x0 = case.x0
        self.xf = case.xf

        self.drifts = case.drifts
        self.Hbends = case.Hbends
        self.Vbends = case.Vbends
        self.quads = case.quads
        self.mindrift = case.mindrift

        self.betx_lim = 400
        self.bety_lim = 400

        self.maxdriftdelta = 30 #maximum drift between elements for v2

        self.element_types = get_element_types(self.drifts, self.Hbends, self.Vbends, self.quads)
        print('element types ', self.element_types)

        self.maxlength = MAXLENGTH #TODO calculate from xyz?
        self.nevals = 0

        # Initilialisation
        self._build_lists()
        self._init_madx_engine()


    def _build_lists(self):
        self.element_base_types = []
        self.element_list = []
        self.element_lengths = []
        self.element_max_strengths = []

        #TODO pass elements together to avoid hardcoding of the order
        for drift in self.drifts:
            for i in range(drift['families']):
                self.element_base_types.append('drift')
                self.element_list.append(drift['name'] + '_' + str(i))
                self.element_lengths.append(drift['length'])
                self.element_max_strengths.append(0)

        for Hbend in self.Hbends:
            for i in range(Hbend['families']):
                self.element_base_types.append('hbend')
                self.element_list.append(Hbend['name'] + '_' + str(i))
                self.element_lengths.append(Hbend['length'])
                self.element_max_strengths.append(Hbend['max int. B'])

        for Vbend in self.Vbends:
            for i in range(Vbend['families']):
                self.element_base_types.append('vbend')
                self.element_list.append(Vbend['name'] + '_' + str(i))
                self.element_lengths.append(Vbend['length'])
                self.element_max_strengths.append(Vbend['max int. B'])

        for quad in self.quads:
            for i in range(quad['families']):
                self.element_base_types.append('quadrupole')
                self.element_list.append(quad['name'] + '_' + str(i))
                self.element_lengths.append(quad['length'])
                self.element_max_strengths.append(quad['max int. G'])

        self._shuffle_chromosome_elements()


    def _init_madx_engine(self, restart=False):
        if restart and self.lowlevel_madx:
            self.madx.quit()
            libmadx.finish()
        elif restart:
            self.madx.quit()
        elif not restart and self.lowlevel_madx:
            time.sleep(np.random.uniform())

        command_log = 'madx_logger' if self.verbose else None
        if self.lowlevel_madx:
            self.madx = Madx(libmadx, command_log=command_log)
        else:
            self.madx = Madx(command_log=command_log)

        if self.suppress_madx_output:
            self.madx.option(echo=False, warn=False, info=False, twiss_print=False)

        self.madx_lats_counter = 0
        self.madx.command.marker.clone('endmarker')


    def _init_sequence(self):

        if self.verbose:
            print('******** initlialise sequence')

        self.madx_lats_counter += 1
        if self.madx_lats_counter > MAX_MADX_LATS_COUNTER:  # restart MADX engine, due to issues after many lattices
            self._init_madx_engine(restart=True)

        #self.s = self.length
        self.real_space_distance_to_target_previous = np.inf
        self.previous_reward = -np.inf

        self.madx.beam(particle='proton', pc='{:10.6e}'.format(self.pc))
        self.madx.command.sequence.clone('s1', l='{:10.6e}'.format(self.maxlength * 2))
        self.madx.command.endsequence()

        self.currents = np.array([], dtype=np.float32)

        # self.madx.use(sequence='s1')
        self.total_cost = 0

        self.generated_FODO = False


    def _decode_chromosome_v1(self):
        """"" based on 3 separate parts
              loc[0:N-1] gives the element family strength, from N families
              loc[1] gives the fractional strength (including 'off')
              loc[2] gives the position in the sequence with respect to the previous element
        """""

        self.rchromosome = round_chr(self.chromosome, self.element_types)
        self.elements = []

        self.s = 0.1

        self.num_elements = int(self.rchromosome[self.element_types])

        for i, rchr in enumerate(self.rchromosome[self.element_types + 1:self.element_types + 1 + self.num_elements]):

            element_type = self.element_list[int(rchr)]
            fractional_strength = self.chromosome[int(rchr)]
            base_type = self.element_base_types[int(rchr)]
            length = self.element_lengths[int(rchr)]
            max_strength = self.element_max_strengths[int(rchr)]

            self.elements.append(element_type)
            #print('type : {}, max_str {}, frac_str : {}, base type : {}, length {}'.format(
                #element_type, max_strength, round(fractional_strength,4), base_type, length))

            self.s += self.element_lengths[int(rchr)] / 2
            self._insert_element(element_type, max_strength, fractional_strength, base_type, length)
            if 'drift' not in element_type:
                self.s += self.element_lengths[int(rchr)] / 2 + self.mindrift
            else:
                self.s += self.element_lengths[int(rchr)] / 2
            #self.s += self.element_lengths[int(rchr)]


    def _decode_chromosome_v2(self):
        """"" based on codons on length 3, where
              loc[0] gives the element type and family
              loc[1] gives the fractional strength (including 'off')
              loc[2] gives the position in the sequence with respect to the previous element
        """""

        self.elements = []

        self.s = 0

        #self.num_elements = int(self.rchromosome[self.element_types])
        self.num_elements = len(self.chromosome) // 3

        for i in range(self.num_elements):

            # coden[0] - element type
            idx = int(self.element_types * (1 + self.chromosome[3 * i])/2-1e-10) # gives value in range 0 - self.element_types
            element_type = self.element_list[idx]
            base_type = self.element_base_types[idx]
            length = self.element_lengths[idx]
            max_strength = self.element_max_strengths[idx]
            self.elements.append(element_type)

            #codon[1] - strength modifier
            if np.abs(self.chromosome[1 + 3 * i] ) < self.relu:
                fractional_strength = 0
            else:
                fractional_strength = np.sign(self.chromosome[1 + 3*i]) * (
                        np.abs(self.chromosome[1 + 3*i]) -  self.relu) / (1. - self.relu)

            #codon[2] - position in sequence
            delta_s = (1 + self.chromosome[2 + 3 * i])/2 * self.maxdriftdelta

            if self._check_seq_length(length, delta_s):
                if np.abs(fractional_strength) > 0:
                    # print('type : {}, max_str {}, frac_str : {}, base type : {}, length {}'.format(
                    # element_type, max_strength, round(fractional_strength,4), base_type, length))

                    # print(self.chromosome[3 * i], self.chromosome[1 + 3 * i], self.chromosome[2 + 3 * i],)
                    # print(i, idx, delta_s, self.s, length)
                    self.s += delta_s + length / 2
                    self._insert_element(element_type, max_strength, fractional_strength, base_type, length)
                    if 'drift' not in element_type:
                        self.s += length / 2 + self.mindrift
                    else:
                        self.s += length / 2


    def _decode_chromosome_v3(self):
        """"" based on codons on length 3, where
              loc[0] gives the element type and family
              loc[1] gives the fractional strength (including 'off')
              loc[2] gives the absolute position in the sequence
              ('is_valid' flag added to signify sequence will parse for madx)
        """""
        self.elements = []
        self.s = 0

        #self.num_elements = int(self.rchromosome[self.element_types])
        self.num_elements = len(self.chromosome) // 3
        self.occupied_s = []
        max_s = 0
        for i in range(self.num_elements):

            idx = int(self.element_types * (1 + self.chromosome[3 * i]-1e-10)/2) # gives value in range 0 - self.element_types
            element_type = self.element_list[idx]
            base_type = self.element_base_types[idx]
            length = self.element_lengths[idx]
            max_strength = self.element_max_strengths[idx]
            self.elements.append(element_type)

            #codon[1] - strength modifier
            if np.abs(self.chromosome[1 + 3 * i]) < self.relu:
                fractional_strength = 0
            else:
                fractional_strength = np.sign(self.chromosome[1 + 3*i]) * (
                        np.abs(self.chromosome[1 + 3*i]) -  self.relu) / (1. - self.relu)

            #codon[2] - absolute position
            self.s = (1 + self.chromosome[2 + 3 * i])/2 * (self.maxlength - 5)

            # print('type : {}, max_str {}, frac_str : {}, base type : {}, length {}'.format(
            # element_type, max_strength, round(fractional_strength,4), base_type, length))

            #print(self.chromosome[3 * i], self.chromosome[1 + 3 * i], self.chromosome[2 + 3 * i],)
            if self._check_slot(length):
                if np.abs(fractional_strength) > 0:
                    max_s = max(max_s, self.s + length/2 + self.mindrift/2)
                    self.occupied_s.append((self.s - length / 2 - self.mindrift / 2, self.s + length / 2 + self.mindrift / 2))
                    self._insert_element(element_type, max_strength, fractional_strength, base_type, length)
                else:
                    pass
                    #print('zero strength on element ', i)
            else:
                pass
                #print('not valid slot for element ', i)

        self.s = max_s

    def _decode_chromosome_v4(self):
        """"" based on composite V1 and V3
              first [0:N-1] loci code for element strength from N families
              next locus [N] defines how many elements are expressed
              next [N+1:2M+1] loci code for M elements: type and location codon
        """""
        self.elements = []
        self.occupied_s = []

        self.s = 0
        max_s = 0

        max_num_elements = int((len(self.chromosome) - self.element_types - 1) / 2)
        self.num_elements_expressed = int((1 + self.chromosome[self.element_types] - 1e-10) / 2 * max_num_elements)
        #print(max_num_elements, self.num_elements_expressed)

        #for i in range(self.element_types + 1, self.element_types + 1 + 2 * self.num_elements_expressed, 2):
        for i in range(self.element_types + 1, len(self.chromosome), 2):

            chr_1 = self.chromosome[i]
            chr_2 = self.chromosome[i+1]

            #element type
            ele_idx = int((1 + chr_1 - 1e-10) / 2 * self.element_types)
            element_type = self.element_list[ele_idx]
            fractional_strength = self.chromosome[ele_idx]
            base_type = self.element_base_types[ele_idx]
            length = self.element_lengths[ele_idx]
            max_strength = self.element_max_strengths[ele_idx]
            self.elements.append(element_type)

            #element location
            self.s = (1 + chr_2) / 2 * (self.maxlength - 5)

            # print('type : {}, max_str {}, frac_str : {}, base type : {}, length {}'.format(
            #     element_type, max_strength, round(fractional_strength, 4), base_type, length))

            if self._check_slot(length):
                max_s = max(max_s, self.s + length / 2 + self.mindrift/2)
                self.occupied_s.append((self.s - length / 2 - self.mindrift/2, self.s + length / 2 + self.mindrift/2))
                self._insert_element(element_type, max_strength, fractional_strength, base_type, length)
            else:
                pass
                #print('not valid slot for element ', i)

        self.s = max_s


        def _decode_chromosome_v5(self):
            """"" based on higher-level abstraction with N_t types of repeating sub-units
                  loc[0] gives the length to first element
                  loc[1] gives the length of cell
                  loc[2] gives 'short' spacing between quads
                  loc[3] gives quad[0] strength
                  loc[4] gives delta on quad[1] strength (quad[1] = -quad[0] + delta)  
                  loc[5] gives number of repetitions of cell
            """""
            pass


    def _check_slot(self, length):
        validity = True
        if (self.s - length / 2. <= 0.0):
            validity = False
        elif (self.s + length / 2. >= self.maxlength):
            validity = False
        else:
            for slot in self.occupied_s:
                sl = slot[1] - slot[0] + 0.601
                sm = (slot[0] + slot[1]) / 2
                if (np.abs(self.s - sm) <= (sl + length) / 2):
                    validity = False
        return validity

    def _check_seq_length(self, length, delta_s):
        if (self.s + delta_s + length + 0.4 >= self.maxlength):
            return False
        return True


    def decode_phenotype(self, chr):
        pass


    def _shuffle_chromosome_elements(self):

        c = list(zip(self.element_base_types, self.element_list, self.element_lengths, self.element_max_strengths))
        np.random.shuffle(c)
        self.element_base_types, self.element_list, self.element_lengths, self.element_max_strengths = zip(*c)


    def _insert_element(self, element_type, max_strength, fractional_strength, base_type, length):

        elem_idx = 0
        while ((element_type + '_{:d}'.format(elem_idx))
               in self.madx.sequence['s1'].elements):
            elem_idx += 1

        if base_type == 'quadrupole':
            self.madx.command.quadrupole.clone(element_type + '_{:d}'.format(elem_idx), l = length,
                                               k1 = max_strength / length / self.beam_rigidity * fractional_strength)
            np.append(self.currents, max_strength * fractional_strength)
            #self.s += np.abs(length)

        elif base_type == 'hbend':
            self.madx.command.sbend.clone(element_type + '_{:d}'.format(elem_idx), l = length,
                                          angle = max_strength / self.beam_rigidity * fractional_strength)
            np.append(self.currents, max_strength * fractional_strength)
            #self.s += np.abs(length)

        elif base_type == 'vbend':
            self.madx.command.sbend.clone(element_type + '_{:d}'.format(elem_idx), l=length, tilt = np.pi/2,
                                          angle = max_strength / self.beam_rigidity * fractional_strength)
            np.append(self.currents, max_strength * fractional_strength)
            #self.s += np.abs(length)

        elif base_type == 'drift':
            self.madx.command.drift.clone(element_type + '_{:d}'.format(elem_idx),
                                          l=(fractional_strength + 1)/2*length)
            #self.s += np.abs(length)
            pass

        else:
            raise NotImplementedError('no element type', base_type)

        self.madx.command.install(element=element_type + '_{:d}'.format(elem_idx), at=self.s)


    def eval(self, chromosome):
        """
        Takes the chromosome from the genetic algorithm
        decodes it into the phenotype (TL sequence), then
        runs madx and evalutes the output against the target
        """
        self.nevals += 1

        self.chromosome = chromosome

        #self._init_madx_parameters()
        self._init_sequence()

        self.madx.command.seqedit(sequence='s1')
        if self.chr_type == 'v1':
            self._decode_chromosome_v1()
        elif self.chr_type == 'v2':
            self._decode_chromosome_v2()
        elif self.chr_type == 'v3':
            self._decode_chromosome_v3()
        elif self.chr_type == 'v4':
            self._decode_chromosome_v4()
        else:
            raise ValueError('chromosome type not known.')
        self.madx.command.install(element='endmarker', at=self.s + 1e-3)
        self.madx.command.endedit()
        self.madx.use(sequence='s1', range='#s/endmarker')

        interp = False #TODO tidy up
        if interp:
            self.madx.select(flag='interpolate', step=0.05)
        else:
            self.madx.select(flag='interpolate', clear=True)

        self.twissall = self.madx.twiss(  # TWISS
            betx=self.twiss0['betx'], alfx=self.twiss0['alfx'],
            bety=self.twiss0['bety'], alfy=self.twiss0['alfy'],
            dx=self.twiss0['dx'], dpx=self.twiss0['dpx'],
            dy=self.twiss0['dy'], dpy=self.twiss0['dpy'])\
            .dframe(columns=['betx', 'alfx', 'bety', 'alfy', 'dx', 'dpx', 'dy', 'dpy', 's', 'l',
                             'angle', 'keyword', 'k1l'])

        self.max_betx = np.max(self.twissall['betx'])
        self.max_bety = np.max(self.twissall['betx'])

        self.twisss = dict(self.twissall.iloc[-1]
                           [['betx', 'alfx', 'bety', 'alfy',
                             'dx', 'dpx', 'dy', 'dpy', 's']])

        self.xall = self.madx.survey(  # SURVEY
            x0=self.x0['x'], y0=self.x0['y'], z0=self.x0['z'],
            theta0=self.x0['theta'], phi0=self.x0['phi'], psi0=self.x0['psi'])\
            .dframe(columns=['x', 'y', 'z', 'theta', 'phi', 'psi'])

        self.xs = dict(self.xall.iloc[-1]
                       [['x', 'y', 'z', 'theta', 'phi', 'psi']])

        # calculate penalty function
        weight_xyz = 10.
        weight_ang = 10000.
        weight_beta = 0.
        weight_dispersion = 0.
        weight_max_beta = 0.
        weight_alf = 0.
        weight_ele = 0.0

        penalty = 0
        a = np.array((self.xs['x'], self.xs['y'], self.xs['z']))
        b = np.array((self.xf['x'], self.xf['y'], self.xf['z']))
        penalty_xyz = np.linalg.norm(a - b) * weight_xyz
        penalty += penalty_xyz

        a = np.array((self.xs['theta'], self.xs['phi'], self.xs['psi']))
        b = np.array((self.xf['theta'], self.xf['phi'], self.xf['psi']))
        penalty_ang = np.linalg.norm(a - b) * weight_ang
        penalty += penalty_ang

        a = np.array((self.twisss['betx'], self.twisss['bety']))
        b = np.array((self.twissf['betx'], self.twissf['bety']))
        penalty_beta = np.linalg.norm(a - b) * weight_beta
        penalty += penalty_beta

        a = np.array((self.twisss['alfx'], self.twisss['alfy']))
        b = np.array((self.twissf['alfx'], self.twissf['alfy']))
        penalty_alf = np.linalg.norm(a - b) * weight_alf
        penalty += penalty_alf

        a = np.array((self.twisss['dx'], self.twisss['dy'], self.twisss['dpx'], self.twisss['dpy']))
        b = np.array((self.twissf['dx'], self.twissf['dy'], self.twissf['dpx'], self.twissf['dpy']))
        penalty_dispersion = np.linalg.norm(a - b) * weight_dispersion
        penalty +=  penalty_dispersion

        if self.max_betx > self.betx_lim:
            penalty_max_betx = weight_max_beta * abs(self.max_betx - self.betx_lim)
        else:
            penalty_max_betx = 0
        if self.max_bety > self.bety_lim:
            penalty_max_bety = weight_max_beta * abs(self.max_bety - self.bety_lim)
        else:
            penalty_max_bety = 0
        penalty += (penalty_max_betx + penalty_max_bety)

        penalty_weight = 0
        for base_type in self.element_base_types:
            if base_type == 'quadrupole':
                penalty_weight += 1
            elif base_type == 'hbend' or base_type == 'vbend':
                penalty_weight += 1
        penalty += penalty_weight*weight_ele

        return penalty_beta + penalty_alf + penalty_dispersion + penalty_xyz + penalty_ang + penalty_max_betx + penalty_max_bety


    def make_plots(self, generation):
        fig_twiss = plt.figure()
        gs = mpl.gridspec.GridSpec(3, 1, height_ratios=[1, 5, 5])

        ax_synoptic = fig_twiss.add_subplot(gs[0])
        ax_beta = fig_twiss.add_subplot(gs[1], sharex=ax_synoptic)
        ax_dispersion = fig_twiss.add_subplot(gs[2], sharex=ax_synoptic)
        plt.setp(ax_beta.get_xticklabels(), visible=False)

        ax_synoptic.axis('off')
        ax_synoptic.set_ylim(-1.2, 1)
        ax_synoptic.plot([0, self.twissall['s'].max()], [0, 0], 'k-')
        draw_synoptic(ax_synoptic, self.twissall)

        ax_beta.set_ylabel(r'$\beta$ (m)')
        ax_beta.plot(self.twissall.s, self.twissall.betx, 'r-', label='horizontal')
        ax_beta.plot(self.twissall.s, self.twissall.bety, 'b-', label='vertical')
        ax_beta.legend()

        ax_dispersion.set_ylabel('Dispersion (m)')
        ax_dispersion.plot(self.twissall.s, self.twissall.dx, 'r-')
        ax_dispersion.plot(self.twissall.s, self.twissall.dy, 'b-')
        ax_dispersion.set_xlabel('s (m)')

        fig_twiss.tight_layout()
        fig_twiss.subplots_adjust(hspace=0.1)
        fig_twiss.savefig(os.path.join(self.run_folder, 'twiss_beta_{:05d}.png'.format(generation)))
        plt.close(fig_twiss)


    def dump_output(self, history, best_x):

        best_twiss = self.twissall[['s', 'l', 'betx', 'alfx', 'bety', 'alfy', 'dx', 'dpx', 'dy', 'dpy']]

        print('**** COMPARISONS, current and target values')
        print('**** space comparison')
        print(('{:>8s} '*6).format('x', 'y', 'z', 'theta', 'phi', 'psi'))
        print('{x:8.1f} {y:8.1f} {z:8.1f} {theta:8.3f} {phi:8.3f} {psi:8.3f} '.format(**self.xs))
        print('{x:8.1f} {y:8.1f} {z:8.1f} {theta:8.3f} {phi:8.3f} {psi:8.3f} '.format(**self.xf))

        print('**** twiss comparison')
        print(('{:>8s} '*9).format('betx', 'alfx', 'bety', 'alfy', 'dx', 'dpx', 'dy', 'dpy', 's'))
        print('{betx:8.1f} {alfx:8.1f} {bety:8.1f} {alfy:8.1f} '.format(**self.twisss),
              '{dx:8.1f} {dpx:8.1f} {dy:8.1f} {dpy:8.1f} {s:8.1f} '.format(**self.twisss))
        print('{betx:8.1f} {alfx:8.1f} {bety:8.1f} {alfy:8.1f} '.format(**self.twissf),
              '{dx:8.1f} {dpx:8.1f} {dy:8.1f} {dpy:8.1f} '.format(**self.twissf))
        #print(self.betx_lim, self.bety_lim, self.betx_lim_reached, self.bety_lim_reached)

        # fn = os.path.join(self.run_folder, 'output.txt')
        # with open(fn, "wb") as csv_file:
        #     writer = csv.writer(csv_file, delimiter=',')
        #     for line in output:
        #         writer.writerow(line)

        fn = os.path.join(self.run_folder, 'best_twiss.txt')
        with open(fn, 'w') as handle:
            best_twiss.to_csv(fn, header=None, sep=',', mode='a', float_format='%.3f', quoting=2)

        fn = os.path.join(self.run_folder, 'best_genome.pkl')
        with open(fn, 'wb') as handle:
            pickle.dump(best_x, handle, protocol=pickle.HIGHEST_PROTOCOL)

        fn = os.path.join(self.run_folder, 'history.pkl')
        with open(fn, 'wb') as handle:
            pickle.dump(history, handle, protocol=pickle.HIGHEST_PROTOCOL)

        # fn = os.path.join(run_folder, 'problem.maker.pkl')
        # with open(fn, 'wb') as handle:
        #     pickle.dump(problem.maker, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def end_print(self):
        print(self.madx.sequence['s1'].elements)
        print(self.elements)
        print(self.xs)
        print(self.xf)
        print(self.twisss)
        print(self.twissf)
        #print(self.betx_lim, self.bety_lim, self.betx_lim_reached, self.bety_lim_reached)

    def close(self):
        self.madx.exit()
