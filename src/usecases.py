from linemaker import MAXLENGTH
import numpy as np
# repository of problems, including magnet dictionary types

class Case:
    def __init__(self, case='test'):

        if case == 'test':
            self.description = {'id': 1, 'text': 'Arbitrary test case used in setting up the code'}
            self.quads = [{'name': 'MQ', 'families': 10, 'base cost': 10, 'length': 2., 'aperture': (0.1, 0.1),
                           'max int. G': 28.8,
                           'max current': 500}]
            self.Hbends = [{'name': 'MBB', 'families': 4, 'base cost': 25, 'length': 6., 'aperture': (0.160, 0.042),
                            'max int. B': 11.2,
                            'max current': 1500}]
            self.Vbends = [{'name': 'MBB', 'families': 1, 'base cost': 25, 'length': 6., 'aperture': (0.160, 0.042),
                            'max int. B': 11.2,
                            'max current': 1500}]
            self.drifts = [{'name': 'drift1', 'families': 10, 'base cost': 0.1, 'length': 1., 'aperture': (0.2, 0.2)},
                           {'name': 'drift2', 'families': 10, 'base cost': 0.0, 'length': 4., 'aperture': (0.2, 0.2)}]
            self.x0 = {'x': 0.0, 'y': 0.0, 'z': 0.0, 'theta': 0.0, 'phi': 0.0, 'psi': 0.0}
            self.xf = {'x': 0.70, 'y': 0.0, 'z': 172.5, 'theta': 0.000, 'phi': 0.000, 'psi': 0.0}
            self.twiss0 = {'betx': 94.66, 'alfx': -1.0905, 'dx': 0.0, 'dpx': 0.0, 'bety': 79.60, 'alfy': 0.9170,
                           'dy': 0.0, 'dpy': 0.0}
            self.twissf = {'betx': 79.60, 'alfx': 0.9170, 'bety': 94.66, 'alfy': -1.0905, 'dx': 0.0, 'dpx': 0.0,
                           'dy': 0.0, 'dpy': 0.0}
            self.mindrift = 0.5
            self.beam_rigidity = 1000
            self.weights = None


        elif case == 'sps_arc_cell':
            self.description = {'id': 2,
                                'text': 'SPS standard arc cell with Q20 optics, from exit MDH.10407 to exit MDH.10607'}
            self.quads = [{'name': 'MQ', 'families': 2, 'base cost': 10, 'length': 3.085, 'aperture': (0.1, 0.1),
                           'max int. G': 22.0 * 3.085,
                           'max current': 500}]
            self.Hbends = [{'name': 'MBB', 'families': 1, 'base cost': 25, 'length': 6.026, 'aperture': (0.160, 0.042),
                            'max int. B': 12.7,
                            'max current': 1500}]
            self.Vbends = ()
            self.drifts = [{'name': 'drift1', 'families': 1, 'base cost': 0.1, 'length': 0.1, 'aperture': (0.2, 0.2)}]

            self.x0 = {'x': -1433.54447, 'z': 2505.36090, 'y': 2402.16359, 'theta': -4.93936897, 'phi': -0.00010450,
                       'psi': 0.0} #MDH.10407.S
            self.xf = {'x': -1371.70792, 'z': 2521.79207, 'y': 2402.15645, 'theta': -5.00693010, 'phi': -0.00011890,
                       'psi': 0.0} #MDH.10607.S

            self.twiss0 = {'betx': 101.649, 'alfx': -2.29237, 'dx': 4.72663, 'dpx': 0.107095,
                           'bety': 21.340, 'alfy': 0.561995, 'dy': 0.0, 'dpy': 0.0}
            self.twissf = {'betx': 101.561, 'alfx': -2.28933, 'bety': 21.3188,
                           'alfy': 0.558651, 'dx': 3.17242376, 'dpx': 0.053800, 'dy': 0.0, 'dpy': 0.0}
            self.beam_rigidity = 450 * 3.335639951
            self.mindrift = 0.4
            self.weights = None


        elif case == 'sps_arc_cell_thin':
            self.description = {'id': 2,
                                'text': 'SPS standard arc cell with Q20 optics, from exit MDH.10407 to exit MDH.10607'}
            self.quads = [{'name': 'MQ', 'families': 2, 'base cost': 10, 'length': 0.01, 'aperture': (0.1, 0.1),
                           'max int. G': 22.0 * 3.085,
                           'max current': 500}]
            self.Hbends = [{'name': 'MBB', 'families': 1, 'base cost': 25, 'length': 0.01, 'aperture': (0.160, 0.042),
                            'max int. B': 12.7,
                            'max current': 1500}]
            self.Vbends = ()
            self.drifts = [{'name': 'drift1', 'families': 1, 'base cost': 0.1, 'length': 0.01, 'aperture': (0.2, 0.2)}]

            self.x0 = {'x': -1433.54447, 'z': 2505.36090, 'y': 2402.16359, 'theta': -4.93936897, 'phi': -0.000112,
                       'psi': 0.0}
            self.xf = {'x': -1371.70792, 'z': 2521.79207, 'y': 2402.15645, 'theta': -5.00693010, 'phi': -0.000112,
                       'psi': 0.0}

            self.twiss0 = {'betx': 101.649, 'alfx': -2.29237, 'dx': 4.72663, 'dpx': 0.107095,
                           'bety': 21.340, 'alfy': 0.561995, 'dy': 0.0, 'dpy': 0.0}
            self.twissf = {'betx': 101.561, 'alfx': -2.28933, 'bety': 21.3188,
                           'alfy': 0.558651, 'dx': 3.17242376, 'dpx': 0.053800, 'dy': 0.0, 'dpy': 0.0}

            self.beam_rigidity = 450 * 3.335639951
            self.mindrift = 0.4
            self.weights = None


        elif case == 'TI2':
            self.description = {'id': 2,
                                'text': 'First 1km of TI2,from STARTTI2 241.391162m, ' +
                                        'until entry MQID.23500 1279.202073m'}
            self.quads = [{'name': 'MQ', 'families': 1, 'base cost': 10, 'length': 1.4, 'aperture': (0.1, 0.1),
                           'max int. G': 53.5 * 1.4,
                           'max current': 530}]
            self.Hbends = [
                {'name': 'MBIH', 'families': 1, 'base cost': 25, 'length': 6.300015, 'aperture': (0.160, 0.042),
                 'max int. B': 1.81 * 6.3, 'max current': 5270}]
            self.Vbends = [
                {'name': 'MBIAV', 'families': 1, 'base cost': 25, 'length': 3.412002, 'aperture': (0.160, 0.042),
                 'max int. B': 1.6 * 3.412002, 'max current': 5270}]
            self.drifts = [{'name': 'drift1', 'families': 1, 'base cost': 0.1, 'length': 0.1, 'aperture': (0.2, 0.2)}]

            self.x0 = {'x': -1864.073870, 'z': 2485.796492, 'y': 2405.617909, 'theta': -4.306021, 'phi': 0.042531,
                       'psi': -0.000714}
            self.xf = {'x': -906.935975, 'z': 2087.628925, 'y': 2399.682889, 'theta': -4.473360, 'phi': -0.018580,
                       'psi': 0.003129}

            self.twiss0 = {'betx': 75.646791, 'alfx': 1.517216, 'dx': 1.572875, 'dpx': -0.005248,
                           'bety': 235.080675, 'alfy': -4.690751, 'dy': -3.952284, 'dpy': -0.063853}

            self.twissf = {'betx': 18.250628, 'alfx': 0.468410, 'dx': 1.192044, 'dpx': -0.027151,
                           'bety': 100.878618, 'alfy': -2.393139, 'dy': 0.008143, 'dpy': 0.000230}

            self.beam_rigidity = 450 * 3.335639951
            self.mindrift = 0.4
            self.weights = None


        elif case == 'flat-FODO':
            self.description = {'id': 1, 'text': 'simple FODO od 63deg'}
            self.quads = [{'name': 'MQ', 'families': 2, 'base cost': 2, 'length': 1.2, 'aperture': (0.1, 0.1),
                           'max int. G': 0.03597509496 * 100 * 3.335639951 * 1.5*2, 'max current': 100}]
            self.Hbends = []
            self.Vbends = []
            self.drifts = [{'name': 'drift1', 'families': 1, 'base cost': 0.1, 'length': 0.1, 'aperture': (0.2, 0.2)}]
            self.x0 = {'x': 0, 'y': 0, 'z': 0, 'theta': 0, 'phi': 0, 'psi': 0}
            self.xf = {'x': 0, 'y': 0, 'z': 30, 'theta': 0, 'phi': 0, 'psi': 0}

            self.twiss0 = {'betx': 49.75, 'alfx': -1.7909, 'dx': 0, 'dpx': 0,
                           'bety': 16.42, 'alfy': 0.6239, 'dy': 0, 'dpy': 0}
            self.twissf = {'betx': 49.75, 'alfx': -1.7909, 'dx': 0, 'dpx': 0,
                           'bety': 16.42, 'alfy': 0.6239, 'dy': 0, 'dpy': 0}
            self.beam_rigidity = 100 * 3.335639951
            self.mindrift = 0.4
            self.weights = None
            self.perfect_chromosome_v3 = np.array([
                [1, 1 / 1.5 * (1 - 0.1) + 0.1, 2 * (0.01 + 0.6) / MAXLENGTH - 1],
                [1, -(1 / 1.5 * (1 - 0.1) + 0.1), 2 * (15 + 0.9) / MAXLENGTH - 1],
                [-1, 1, 2 * (30) / MAXLENGTH - 1],
                [0.] * 3, [0.] * 3]).flatten()  # with 0.1 relu and needs correct padding

        elif case == 'DBA':
            self.description = {'id': 1, 'text': 'double bend achromat of 283/163 deg, max disp of 0.14 for 2x50mrad'}
            self.quads = [{'name': 'MQ', 'families': 2, 'base cost': 2, 'length': 0.5, 'aperture': (0.1, 0.1),
                           'max int. G': 0.4817616519 * 100 * 3.335639951 * 1.5, 'max current': 100}]
            self.Hbends = [{'name': 'MBIH', 'families': 1, 'base cost': 25, 'length': 2, 'aperture': (0.160, 0.042),
                            'max int. B': 50e-3 * 100 * 3.335639951 * 1.5, 'max current': 100}]
            self.Vbends = []
            self.drifts = [{'name': 'drift1', 'families': 1, 'base cost': 0.1, 'length': 0.1, 'aperture': (0.2, 0.2)}]
            self.x0 = {'x': 0, 'y': 0, 'z': 0, 'theta': 0, 'phi': 0, 'psi': 0}
            self.xf = {'x': -1.048688092, 'y': 0, 'z': 20.95628078, 'theta': -0.1, 'phi': 0, 'psi': 0}

            self.twiss0 = {'betx': 29.72492053, 'alfx': 0, 'dx': 0, 'dpx': 0,
                           'bety': 14.51178061, 'alfy': 0, 'dy': 0, 'dpy': 0}
            self.twissf = {'betx': 29.72492053, 'alfx': 0, 'dx': 0, 'dpx': 0,
                           'bety': 14.51178061, 'alfy': 0, 'dy': 0, 'dpy': 0}
            self.beam_rigidity = 100 * 3.335639951
            self.mindrift = 0.4
            self.weights = None
            self.perfect_chromosome_v3 = np.array(
                [[-1, 0.39 / 0.48 / 1.5 * (1 - 0.1) + 0.1, 2 * (4 + 0.25) / (MAXLENGTH - 5) - 1],  # doulet F
                 [0.25, -(1 / 1.5 * (1 - 0.1) + 0.1), 2 * (5.5 + 0.25) / (MAXLENGTH - 5) - 1],  # doublet D
                 [1, (1 / 1.5 * (1 - 0.1) + 0.1), 2 * (6.5 + 1 + 0.15) / (MAXLENGTH - 5) - 1],  # dipole
                 [-1, 0.379 / 0.48 / 1.5 * (1 - 0.1) + 0.1, 2 * (10 + 0.25 - 0.2) / (MAXLENGTH - 5) - 1],  # echromat F
                 [-1, 0.379 / 0.48 / 1.5 * (1 - 0.1) + 0.1, 2 * (10.5 + 0.25 + 0.61 - 0.2) / (MAXLENGTH - 5) - 1], # achromat F
                 [1, (1 / 1.5 * (1 - 0.1) + 0.1), 2 * (12.5 + 1) / (MAXLENGTH - 5) - 1],  # dipole
                 [0.25, -(1 / 1.5 * (1 - 0.1) + 0.1), 2 * (15.1 + 0.25 + 0.01) / (MAXLENGTH - 5) - 1],  # doublet D
                 [-1, 0.39 / 0.48 / 1.5 * (1 - 0.1) + 0.1, 2 * (16.5 + 0.25) / (MAXLENGTH - 5) - 1],  # doublet F
                 [-0.25, 1, 2 * (21) / (MAXLENGTH - 5) - 1],  # end drift
                 [0] * 3]).flatten()
        else:
            raise ValueError('Use case not defined')

        print('twiss0', self.twiss0)
        print('twissf', self.twissf)
        print('x0', self.x0)
        print('xf', self.xf)
