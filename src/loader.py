import os
import pickle
import argparse

def main():
    parser = argparse.ArgumentParser(description='load previous run')
    parser.add_argument('--run_name', default='v2_2020-09-19_11-01-19', help='folder containing data', type=str)
    args, other_args = parser.parse_known_args()

    # some file handling stuff
    run_folder = os.path.join('../data/run', args.run_name)

    fn = os.path.join(run_folder, 'args.pkl')
    with open(fn, 'rb') as handle:
        args = pickle.load(handle)

    for v in vars(args):
        print(v, getattr(args,v))

if __name__ == "__main__":
    main()
