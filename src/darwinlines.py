import argparse
from pymoo.model.problem import Problem
import numpy as np
from pymoo.algorithms.so_de import DE
from pymoo.algorithms.so_genetic_algorithm import GA
from pymoo.algorithms.nsga2 import NSGA2
from pymoo.algorithms.unsga3 import UNSGA3
from pymoo.algorithms.so_nelder_mead import NelderMead
from pymoo.algorithms.so_brkga import BRKGA
from pymoo.algorithms.so_cmaes import CMAES
from pymoo.algorithms.so_pattern_search import PatternSearch
from pymoo.algorithms.so_pso import PSO
from pymoo.factory import get_sampling, get_crossover, get_mutation
from pymoo.factory import get_termination
from pymoo.optimize import minimize
from pymoo.operators.sampling.latin_hypercube_sampling import LatinHypercubeSampling
from pymoo.visualization.scatter import Scatter
from util import get_element_types, round_chr, init_chromosome_v1, init_chromosome_v2, init_chromosome_v4, make_movies, colors
from linemaker import linemaker
import pickle
import datetime
import os
import sys
import time
from multiprocessing import Pool
from functools import partial
from usecases import Case
import matplotlib.pyplot as plt
import matplotlib as mpl
from distutils.util import strtobool

np.random.seed(99)

#TODO
# benchmark tests
# simple FODO of a few cells, no bend
# achromat bend
# arc, with significant bending angle only horizontal
# dogleg
# transport to target, meaning alpha=0 and small equal betas, no bends
# one long problem, like the FCC

#case = Case('sps_arc_cell_thin')
#case = Case('sps_arc_cell')
case = Case('TI2')

class MyProblem(Problem):

    def __init__(self, chrlength, chr_lims, run_folder, case, parsed_args,
                 *args, **kwargs):
        super().__init__(n_var=chrlength,
                         n_obj=1,
                         n_constr=0,
                         xl = chr_lims[0],
                         xu = chr_lims[1])

        self.maker = linemaker
        self.chrlength = chrlength
        self.plot_every = parsed_args.plot_every
        self.run_folder = run_folder
        self.parsed_args = parsed_args
        self.history = [[],[]]
        self.generation = 0
        self.pool = Pool(self.parsed_args.pool_size)
        self.history_fig, _ = plt.subplots(2, 1, tight_layout=True, sharex='all')

        # history plot initialisation
        objectives_names = ['obj {:2d}'.format(i) for i in range(1, self.n_obj+1)]
        self.history_fig.axes[0].legend(handles=[
            mpl.patches.Patch(color=colors[index],
                              label='{:s}'.format(objectives_names[index]))
            for index in range(self.n_obj)
        ])
        self.history_fig.axes[0].set_yscale('log')
        self.history_fig.axes[1].set_yscale('log')
        self.history_fig.axes[0].set_ylabel('minimum')
        self.history_fig.axes[1].set_ylabel('mean')
        self.history_fig.axes[1].set_xlabel('generation')

        # initialization of the pool of processes, each with one linemaker object
        _ = self.pool.map(
            partial(self._linemaker_process_init,
                    run_folder=self.run_folder, chr_type=self.parsed_args.chr_type, relu=self.parsed_args.relu,
                    lowlevel_madx=self.parsed_args.low_level_madx, case=case),
            [i for i in range(self.parsed_args.pool_size)]
        )

    @staticmethod
    def _linemaker_process_init(_, run_folder, chr_type, relu,
                                lowlevel_madx, case):
        global maker # trick to keep the variable in calls of the process
        maker = linemaker(run_folder, chr_type, case, relu, lowlevel_madx)

    @staticmethod
    def _linemaker_process_eval(chromosome):
        return maker.eval(chromosome)

    @staticmethod
    def _linemaker_process_eval_plot_dump_print(
            eval=False, make_plots=False, dump_output=False, end_print=False,
            chromosome=None, history=None, generation=None
    ):
        if eval:
            maker.eval(chromosome)
        if make_plots:
            maker.make_plots(generation)
        if dump_output:
            maker.dump_output(history, chromosome)
        if end_print:
            maker.end_print()

    def _evaluate(self, chr, out, *args, **kwargs):

        self.generation += 1

        res = np.array(self.pool.map(
            self._linemaker_process_eval,
            chr))
        err = np.stack(res[:, 0]) if self.n_constr > 0 else res
        err_summed = err if self.n_obj == 1 else err.sum(axis=1)
        constraints = np.stack(res[:, 1]) if self.n_constr > 0 else None
        this_best_chr = chr[np.argmin(err_summed)]

        if self.plot_every > 0 and self.generation > 2 and self.generation % self.plot_every == 0:
            if np.min(err_summed) < np.min(self.history[0]):
                self.best_chr = this_best_chr
            self.pool.apply(self._linemaker_process_eval_plot_dump_print,
                            kwds=dict(eval=True, make_plots=True, dump_output=True,
                                      chromosome=this_best_chr, history=self.history, generation=self.generation)
            )
            self.history_fig.savefig(os.path.join(self.run_folder, 'history.png'))

        self.history[0].append(np.min(err_summed))
        self.history[1].append(np.mean(err_summed))
        out["F"] = err
        out["G"] = constraints

        print('gen.# : {:5d}, all-time-best : {:8.2e}, gen.min : {:8.2e}, gen.aver : {:8.2e}'.format(
            self.generation, np.min(self.history[0]), np.min(err_summed), np.mean(err_summed)))

        if self.generation % 5 == 0:
            minimum = err[np.argmin(err_summed)]
            mean = err.mean(axis=0)
            for index, val in enumerate([minimum] if self.n_obj == 1 else minimum):
                self.history_fig.axes[0].plot(self.generation, val, color=colors[index], marker='.', alpha=0.5)
            for index, val in enumerate([mean] if self.n_obj == 1 else mean):
                self.history_fig.axes[1].plot(self.generation, val, color=colors[index], marker='.', alpha=0.5)


def main():

    parser = argparse.ArgumentParser(description='Use genetic algorithm for TL design')

    # genetic algorithm parameters
    parser.add_argument('--algorithm', default='CMAES', help='Genetic Algorithm', type=str)
    parser.add_argument('--gen', default=2000,  help='Number of generations', type=int)
    parser.add_argument('--pop', default=500, help='Population per generation', type=int)
    parser.add_argument('--relu', default=0.1, help='amplitude of relu plateau around zero for strength', type=float)

    # for sampling (several algorithms)
    parser.add_argument('--iter', default=3, help='Number of iterations', type=int)

    # for CMAES algorithm
    parser.add_argument('--restarts', default=5, help='Restarts', type=int)
    parser.add_argument('--CMA_elitist', default=False, help='elitism flag', type=lambda x: bool(strtobool(x)))

    # for DE algorithm
    parser.add_argument('--CR', default=0.5, help='Exchange probability', type=float) #0.5
    parser.add_argument('--F', default=0.3, help='Crossover_weight', type=float) #0.3

    # chromosome type and length parameters
    parser.add_argument('--chr_type', default='v3', help='genome decoding protocol', type=str)
    parser.add_argument('--num_e', default=40, help='number of element genes in genome', type=int)

    # penalty function limits
    # parser.add_argument('--betalim', default=200, help='penalty limit for betax/y', type=float)
    parser.add_argument('--low_level_madx', default=False, type=lambda x: bool(strtobool(x)),
                        help='going low-level for high performance but messy output')

    # plotting and logging
    parser.add_argument('--plot_every', default=50, help='save intermediate results every n generations (0 = never)', type=int)

    # pool size for the madx processes
    parser.add_argument('--pool_size', default=8, help='Pool size for parallel madx jobs', type=int)
    parser.add_argument('--name_prepend', default='', help='Prepent string to output directory', type=str)

    args, other_args = parser.parse_known_args()

    # some file handling stuff
    run_folder = os.path.join(
        '../data/run',
        args.name_prepend + '_' + args.algorithm + '_' +
        args.chr_type + '_' + datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))
    os.makedirs(run_folder)

    with open(os.path.join(run_folder, 'args.pkl'), 'wb') as handle:
        pickle.dump(args, handle, protocol=pickle.HIGHEST_PROTOCOL)
    with open(os.path.join(run_folder, 'args.txt'), 'wt') as handle:
        handle.write('\n'.join(sys.argv[1:]))

    # initilise chromosome #TODO hide this inside the linemaker class
    if args.chr_type == 'v1':
        chr, chr_lims, chr_stds, element_types = init_chromosome_v1(
            args.num_e, case.drifts, case.Hbends, case.Vbends, case.quads)
    elif args.chr_type == 'v2' or args.chr_type == 'v3':
        dummydrifts = () #TODO correct drifts variable still passed to V2/3 if this line not included
        chr, chr_lims, chr_stds, element_types = init_chromosome_v2(
            args.num_e, dummydrifts, case.Hbends, case.Vbends, case.quads)
    elif args.chr_type == 'v4':
        chr, chr_lims, chr_stds, element_types = init_chromosome_v4(
            args.num_e, case.drifts, case.Hbends, case.Vbends, case.quads)
    else:
        raise ValueError('chromosome type not known.')

    problem = MyProblem(len(chr), chr_lims, run_folder, case, args)

    if args.algorithm == 'DE': #as is only for 1 objective
        algorithm = DE(
            pop_size=args.pop,
            sampling=LatinHypercubeSampling(iterations=args.iter, criterion="maxmin"),
            variant="DE/rand/1/bin",
            CR=args.CR,
            F=args.F,
            dither="vector",
            jitter=False
        )

    elif args.algorithm == 'GA': #as is only for 1 objective
        algorithm = GA(
            pop_size=args.pop,
            sampling=LatinHypercubeSampling(iterations=args.iter, criterion="maxmin"),
            crossover=get_crossover("real_two_point"),
            mutation=get_mutation("real_pm"),
            eliminate_duplicates=True)

    elif args.algorithm == 'NSGA2':
        algorithm = NSGA2(pop_size=args.pop,
            sampling=LatinHypercubeSampling(iterations=args.iter, criterion="maxmin"),
            crossover=get_crossover("real_two_point"),
            mutation=get_mutation("real_pm"),
            eliminate_duplicates=True)

    elif args.algorithm == 'NelderMead':
        algorithm = NelderMead()

    elif args.algorithm == 'PatternSearch':
        algorithm = PatternSearch()

    elif args.algorithm == 'UNSGA3':
        ref_dirs = np.array([[1.0]])
        algorithm = UNSGA3(ref_dirs, pop_size=args.pop)

    elif args.algorithm == 'BRKGA':
        algorithm = BRKGA(
            n_elites=int(0.25 * args.pop),
            n_offsprings=args.pop,
            n_mutants=int(0.15 * args.pop),
            bias=0.7)

    elif args.algorithm == 'CMAES':
        algorithm = CMAES(
            sigma=0.5,
            CMA_stds=chr_stds,
            cmaes_verbose=-9,
            popsize=args.pop,
            CMA_elitist=args.CMA_elitist)

    elif args.algorithm == 'PSO':
        algorithm = PSO(
            pop_size=args.pop,
            sampling=LatinHypercubeSampling(iterations=args.iter, criterion="maxmin"),)
    termination = get_termination("n_gen", args.gen)

    print("Starting {} with genome {}, for {} generations and {} individuals per generation".
          format(args.algorithm, args.chr_type, args.gen, args.pop))

    res = minimize(problem,
                   algorithm,
                   termination,
                   seed=7,
                   verbose=False,
                   save_history=False)
    if res.X.ndim==2:
        best_index = np.argmin(res.F.sum(axis=1))
        best_chr = res.X[best_index]
    else:
        best_chr = res.X
    problem.pool.apply(
        problem._linemaker_process_eval_plot_dump_print,
        kwds=dict(dump_output=True, make_plots=True, end_print=True, eval=True,
                  history=problem.history, generation=args.gen+1, chromosome=best_chr)
    )

    plot = Scatter(tight_layout=True, alpha=0.5)
    plot.add(res.F)
    plot.save(os.path.join(run_folder, 'objectives.png'))

    with open(os.path.join(run_folder, 'res.pckl'), 'wb') as handle:
        pickle.dump({'F': res.F, 'G': res.G, 'CV': res.CV, 'X': res.X, 'exec time':res.exec_time},
                    handle, protocol=pickle.HIGHEST_PROTOCOL)

    #make_movies(run_folder)

if __name__ == "__main__":
    start_time = time.time()
    main()
    print('execution took {:8.1f} s'.format(time.time()-start_time))
