import numpy as np
import os
import cv2
import pandas as pd

from matplotlib import pyplot as plt
import matplotlib as mpl

colors = [plt.get_cmap('hsv')(1. * i/7) for i in range(7)]

def check_current(currents, current, max_current, margin=5e-3):
    """
    Checks if the current is in currents within a
    relative margin of margin
    returns if existing and index
    """

    if len(currents) == 0:
        return False, np.NaN

    idx = (np.abs(currents - np.abs(current))).argmin()

    if np.abs((currents[idx] - np.abs(current)) / max_current) < margin:
        return True, idx
    elif np.abs(current / max_current) < margin:
        return True, -1
    else:
        return False, np.NaN


def perfect_fodo_start_twiss(mu, l_cell, klmax=np.inf, fodo_entrance_foc=True):
    """  returns the beam parameters at the start of a bendless FODO"""
    kl = 4 * np.sin(mu / 2) / l_cell
    if np.abs(kl) > klmax * 0.8:
        #kl = klmax * np.random.uniform(0.2, 0.8, 1).item()
        kl = klmax * 0.8
        mu = 2 * np.arcsin(kl * l_cell / 4)

    if (np.abs(kl) + 0.2 * klmax) > 4 / l_cell:
        kl = (4 / l_cell - 0.1 * klmax) * np.sign(kl)
        mu = 2 * np.arcsin(kl * l_cell / 4)

    beta1 = np.abs(l_cell / np.sin(mu) * (1 + np.sin(mu / 2)))
    alpha1 = (-1 - np.sin(mu / 2)) / np.cos(mu / 2)

    beta2 = np.abs(l_cell / np.sin(mu) * (1 - np.sin(mu / 2)))
    alpha2 = (+1 - np.sin(mu / 2)) / np.cos(mu / 2)

    if fodo_entrance_foc:
        return alpha1, beta1, alpha2, beta2, kl, mu
    else:
        return alpha2, beta2, alpha1, beta1, kl, mu


def norm_parm(xmin, xmax, x, clip=False, clipmin=None, clipmax=None):
    x = (x - xmin) / (xmax - xmin) * 2 - 1

    if clip:
        x = np.clip(x, clipmin, clipmax)

    return x

def get_element_types(*kwargs):
    # counts number of distinct families (including drifts)
    element_types = 0

    for element_type in kwargs:
        for element in element_type:
            for _ in range(element['families']):
                element_types += 1
    return element_types

def round_chr(chr, element_types):
    rchr = np.zeros(len(chr))
    rchr[0:element_types] = chr[0:element_types]
    rchr[element_types:-1] = [int(ch) for ch in chr[element_types:-1]]
    return rchr


def init_chromosome_v1(num_elements, *kwargs):
    """""
    composite chromosome
    with strengths in first Ns locations,
    followed by one 'length' gene,
    then with Ne element type genes
    """""
    element_types = get_element_types(*kwargs)
    chromosome = []
    chromosome.extend(np.random.uniform(-1, 1, size=(element_types)))
    chromosome.extend(np.random.uniform(0, num_elements, size = (1)))
    chromosome.extend(np.random.uniform(0, element_types, size=(num_elements)))

    chromosome_l = element_types * [-1]
    chromosome_u = element_types * [1]
    chromosome_l.extend(np.asarray((1,)))
    chromosome_u.extend(np.asarray((num_elements - 1,)))
    chromosome_l.extend(np.zeros(num_elements))
    chromosome_u.extend((element_types - 1) * np.ones(num_elements))
    chromosome_stds = [a/10 - b/10 for a, b in zip(chromosome_u, chromosome_l)]

    chromosome_limits = [chromosome_l, chromosome_u]

    print('chromosome length : ',len(chromosome))

    return(chromosome, chromosome_limits, chromosome_stds, element_types)


def init_chromosome_v2(num_elements, *kwargs):
    """""
    codon-based chromosome
    with type, strength and position (absolute, or -from-last-element) coded 
    into three adjacent genome locations
    """""
    element_types = get_element_types(*kwargs)

    chromosome = []
    chromosome.extend(np.random.uniform(-1, 1, size=(3 * num_elements)))

    chromosome_l = len(chromosome) * [-1]
    chromosome_u = len(chromosome) * [1]
    chromosome_stds = len(chromosome) * [1]
    chromosome_limits = [chromosome_l, chromosome_u]

    print('chromosome length : ',len(chromosome))

    return(chromosome, chromosome_limits, chromosome_stds, element_types)


def init_chromosome_v4(num_elements, *kwargs):
    """""
    composite chromosome (all floats in range [-1:1])
    with strengths in first Ns locations,
    followed by one 'length' gene,
    then with 2xNe element [type + location] genes
    """""
    element_types = get_element_types(*kwargs)
    chromosome = []
    chromosome.extend(np.random.uniform(-1, 1, size=(element_types + 1 + 2* num_elements)))

    chromosome_l = len(chromosome) * [-1]
    chromosome_u = len(chromosome) * [1]
    chromosome_stds = len(chromosome) * [1]
    chromosome_limits = [chromosome_l, chromosome_u]

    print('chromosome length : ',len(chromosome))

    return(chromosome, chromosome_limits, chromosome_stds, element_types)

def make_movies(image_folder):

    video_name = 'twiss_video.avi'

    images = [img for img in os.listdir(image_folder) if (img.endswith(".png") and img.startswith("twiss_beta"))]
    frame = cv2.imread(os.path.join(image_folder, images[0]))
    height, width, layers = frame.shape

    video = cv2.VideoWriter(video_name, 0, 1, (width, height))

    for image in images:
        video.write(cv2.imread(os.path.join(image_folder, image)))

    cv2.destroyAllWindows()
    video.release()

def draw_synoptic(ax, twiss):
    for _, row in twiss.iterrows():
        if row['keyword'] == 'quadrupole':
            _ = ax.add_patch(
                mpl.patches.Rectangle(
                    (row['s']-row['l'], 0), row['l'], np.sign(row['k1l']),
                    facecolor='k', edgecolor='k'))
        elif (row['keyword'] == 'rbend' or
              row['keyword'] == 'sbend'):
            _ = ax.add_patch(
                mpl.patches.Rectangle(
                    (row['s']-row['l'], -1), row['l'], 2,
                    facecolor='None', edgecolor='r' if np.sign(row['angle']) < 0 else 'b'))
