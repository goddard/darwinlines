Automatic transfer line design using Darwinian genetic algorithms

- Genome as list of floats, coding information about element types, lengths, strengths
- Different versions of genome decoding into madx sequence
- Genome values all floats in range (-1,1) and expressed into phenotype by custom fucntions
- Madx sequence checked for validity as elements are expressed (overlaps, length)
- Twiss run and output parameters including survey generated
- Penalty scaler (or vector) constructed, which also includes 'costs' if exceeding limits or for numbers  of magnets
- Weighting factors adjusted by hand for the different components
- Possible to use multi-objective function, refactoring needed
- Pymoo algorithm of choice for optimising the penalty

- With CMAES see convergence with pop 200 and 2000 generations (400k madx calls)
- Larger population size seems advantageous
- Elitism seems to trap algorithm in local minimum, worse overall solution
